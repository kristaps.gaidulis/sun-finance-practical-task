<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Card
 * @ORM\Table(name="card", uniqueConstraints={@ORM\UniqueConstraint(name="number", columns={"number"})}, indexes={@ORM\Index(name="status_key", columns={"status_key"})})
 * @ORM\Entity(repositoryClass="App\Repository\CardRepository")
 */
class Card
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=16, nullable=false, unique=true)
     *
     * @Assert\NotBlank(message="Card number cant be blank")
     * @Assert\Length(
     *     min = 16,
     *     max = 16,
     *     exactMessage="Card number must be exactly 16 digits long"
     * )
     * @Assert\Type(
     *     type="digit",
     *     message="Only digits allowed in message"
     * )
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="serie", type="string", length=4, nullable=true)
     *
     * @Assert\Length(
     *     min = 4,
     *     max = 4,
     *     exactMessage="Card number must be exactly 4 digits long"
     * )
     * @Assert\Type(
     *     type="digit",
     *     message="Only digits allowed in series number"
     * )
     */
    private $serie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_expiry", type="datetime", nullable=true)
     *
     * @Assert\GreaterThan(
     *     value = "today",
     *     message = "Expiration date must be set in future"
     * )
     */
    private $dateOfExpiry;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_use", type="datetime", nullable=true)
     */
    private $dateOfUse;

    /**
     * @var float
     *
     * @ORM\Column(name="sum", type="float", precision=10, scale=0, nullable=false)
     *
     * @Assert\GreaterThanOrEqual(
     *     value= 0,
     *     message= "Card balance cant be negative"
     * )
     */
    private $sum = '0';

    /**
     * @var \CardStatus
     *
     * @ORM\ManyToOne(targetEntity="CardStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_key", referencedColumnName="id")
     * })
     */
    private $statusKey;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getDateOfExpiry(): ?\DateTimeInterface
    {
        return $this->dateOfExpiry;
    }

    public function setDateOfExpiry(?\DateTimeInterface $dateOfExpiry): self
    {
        $this->dateOfExpiry = $dateOfExpiry;

        return $this;
    }

    public function getDateOfUse(): ?\DateTimeInterface
    {
        return $this->dateOfUse;
    }

    public function setDateOfUse(?\DateTimeInterface $dateOfUse): self
    {
        $this->dateOfUse = $dateOfUse;

        return $this;
    }

    public function getSum(): ?float
    {
        return $this->sum;
    }

    public function setSum(float $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getStatusKey(): ?CardStatus
    {
        return $this->statusKey;
    }

    public function setStatusKey(?CardStatus $statusKey): self
    {
        $this->statusKey = $statusKey;

        return $this;
    }
}

