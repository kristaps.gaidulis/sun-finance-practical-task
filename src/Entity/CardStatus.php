<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CardStatus
 *
 * @ORM\Table(name="card_status")
 * @ORM\Entity(repositoryClass="App\Repository\CardStatusRepository")
 */
class CardStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=20, nullable=true)
     */
    private $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }


}

