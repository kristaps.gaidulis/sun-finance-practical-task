<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transactions
 *
 * @ORM\Table(name="transactions", indexes={@ORM\Index(name="product_key", columns={"product_key"}), @ORM\Index(name="card_key", columns={"card_key"})})
 * @ORM\Entity
 */
class Transactions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="purchase_date", type="datetime", nullable=false)
     */
    private $purchaseDate;

    /**
     * @var \Products
     *
     * @ORM\ManyToOne(targetEntity="Products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_key", referencedColumnName="id")
     * })
     */
    private $productKey;

    /**
     * @var \Card
     *
     * @ORM\ManyToOne(targetEntity="Card")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="card_key", referencedColumnName="id")
     * })
     */
    private $cardKey;

    public function getId(): int
    {
        return $this->id;
    }

    public function getPurchaseDate(): ?\DateTime
    {
        return $this->purchaseDate;
    }

    /**
     * @param \DateTime $purchaseDate
     */
    public function setPurchaseDate(\DateTime $purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;
    }

    public function getCardKey(): ?Card
    {
        return $this->cardKey;
    }

    public function setCardKey(?Card $cardKey)
    {
        $this->cardKey = $cardKey;
    }

    public function getProductKey(): ?Products
    {
        return $this->productKey;
    }

    public function setProductKey(?Products $productKey)
    {
        $this->productKey = $productKey;
    }
}

