<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Card
 *
 * @ORM\Table(name="products")
 * @ORM\Entity()
 */
class Products
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     *
     * @Assert\Length(
     *     min = 4,
     *     max = 50,
     *     minMessage="product title must be attleast 4 symbols long",
     *     maxMessage="maximum 50 symbols in product title"
     * )
     */
    private $title;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     *
     * @Assert\GreaterThanOrEqual(
     *     value= 0,
     *     message= "Card balance cant be negative"
     * )
     */
    private $price = '0';


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price)
    {
        $this->price = $price;
    }
}

