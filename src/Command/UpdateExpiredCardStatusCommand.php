<?php
/**
 * Created by PhpStorm.
 * User: Roberts
 * Date: 02.12.2018
 * Time: 15:12
 */

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateExpiredCardStatusCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:update-expired-card-status';

    protected function configure()
    {
        $this->setDescription('Updates card status to expired for all cards which have expiration date > today');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $today = date_format(new \DateTime(),'Y-m-d H:i:s');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $expiredStatus = $em->createQueryBuilder()
            ->select('s')
            ->from('App:CardStatus', 's')
            ->where('s.code = :expired')
            ->setParameter('expired', 'expired')
            ->getQuery()
            ->execute();

        $cardsPastExpirationDate = $em->createQueryBuilder()
            ->select('c')
            ->from('App:Card', 'c')
            ->where('c.dateOfExpiry < :today')
            ->andWhere('c.statusKey != :expiredStatus')
            ->setParameter('today', $today)
            ->setParameter('expiredStatus', $expiredStatus)
            ->getQuery()
            ->execute();

        foreach ($cardsPastExpirationDate as $card) {
            $card->setStatusKey($expiredStatus[0]);

            $em->persist($card);
        }

        $em->flush();
    }
}