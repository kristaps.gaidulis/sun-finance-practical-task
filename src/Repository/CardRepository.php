<?php

namespace App\Repository;

use App\Entity\Card;
use App\Entity\CardStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Card|null find($id, $lockMode = null, $lockVersion = null)
 * @method Card|null findOneBy(array $criteria, array $orderBy = null)
 * @method Card[]    findAll()
 * @method Card[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Card::class);
    }

    public function findHighestCardNumberForSeries($serie)
    {
        return $this->createQueryBuilder('c')
            ->select('c.number')
            ->where('c.serie = :series')
            ->orderBy('c.id', 'DESC')
            ->setParameter('series', $serie)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function createSearchQuery($searchParams)
    {
        $queryString = "SELECT c FROM App:Card c";
        $whereString = " WHERE 1=1";

        if (!is_null($searchParams->card_number) && $searchParams->card_number != '') {

            $whereString .= " AND c.number = :number";
            $params['number'] = $searchParams->card_number;
        }

        if (!is_null($searchParams->card_series) && $searchParams->card_series != '') {
            $whereString .= " AND c.serie = :series";
            $params['series'] = $searchParams->card_series;
        }

        if (!is_null($searchParams->balance_value) && !is_null($searchParams->balance_comparison) && $searchParams->balance_value != '') {
            if ($searchParams->balance_comparison == 'gt') {
                $whereString .= " AND c.sum > :balance";
            } elseif ($searchParams->balance_comparison == 'lt') {
                $whereString .= " AND c.sum < :balance";
            } else {
                $whereString .= " AND c.sum = :balance";
            }

            $params['balance'] = $searchParams->balance_value;
        }

        if (!is_null($searchParams->exp_date_value) && !is_null($searchParams->exp_date_comparison) && $searchParams->exp_date_value != '') {
            if ($searchParams->exp_date_comparison == 'gt') {
                $whereString .= " AND c.dateOfExpiry > :expirationDate";
            } elseif ($searchParams->exp_date_comparison == 'lt') {
                $whereString .= " AND c.dateOfExpiry < :expirationDate";
            } else {
                $whereString .= " AND c.dateOfExpiry = :expirationDate";
            }

            $params['expirationDate'] = $searchParams->exp_date_value;
        }

        if (!is_null($searchParams->use_date_value) && !is_null($searchParams->use_date_comparison) && $searchParams->use_date_value != '') {
            if ($searchParams->use_date_comparison == 'gt') {
                $whereString .= " AND c.dateOfUse > :useDate";
            } elseif ($searchParams->use_date_comparison == 'lt') {
                $whereString .= " AND c.dateOfUse < :useDate";
            } else {
                $whereString .= " AND c.dateOfUse = :useDate";
            }

            $params['useDate'] = $searchParams->use_date_value;
        }

        if (!is_null($searchParams->status) && $searchParams->status != 'any') {
            $status = $this->getEntityManager()->getRepository(CardStatus::class)
                ->findOneBy(['code' => $searchParams->status]);

            $whereString .= " AND c.statusKey = :status";
            $params['status'] = $status;
        }

        $query = $queryString.$whereString;

        $query = $this->getEntityManager()->createQuery($query)->setParameters($params);
        $result = $query->getResult();

        return $result;
    }
}
