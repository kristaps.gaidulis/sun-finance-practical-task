<?php

namespace App\Repository;

use App\Entity\CardStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardStatus[]    findAll()
 * @method CardStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardStatusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardStatus::class);
    }

    // /**
    //  * @return CardStatus[] Returns an array of CardStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardStatus
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
