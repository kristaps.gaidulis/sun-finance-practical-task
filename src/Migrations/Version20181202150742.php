<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181202150742 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf  ($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cron_job (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(191) NOT NULL, command VARCHAR(1024) NOT NULL, schedule VARCHAR(191) NOT NULL, description VARCHAR(191) NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX un_name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cron_report (id INT AUTO_INCREMENT NOT NULL, job_id INT DEFAULT NULL, run_at DATETIME NOT NULL, run_time DOUBLE PRECISION NOT NULL, exit_code INT NOT NULL, output LONGTEXT NOT NULL, INDEX IDX_B6C6A7F5BE04EA9 (job_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cron_report ADD CONSTRAINT FK_B6C6A7F5BE04EA9 FOREIGN KEY (job_id) REFERENCES cron_job (id)');
        $this->addSql('ALTER TABLE card DROP FOREIGN KEY card_ibfk_1');
        $this->addSql('ALTER TABLE card CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE status_key status_key INT DEFAULT NULL, CHANGE number number VARCHAR(16) NOT NULL, CHANGE sum sum DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE card ADD CONSTRAINT FK_161498D354FFFB0B FOREIGN KEY (status_key) REFERENCES card_status (id)');
        $this->addSql('ALTER TABLE card RENAME INDEX number TO UNIQ_161498D396901F54');
        $this->addSql('ALTER TABLE card_status CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cron_report DROP FOREIGN KEY FK_B6C6A7F5BE04EA9');
        $this->addSql('DROP TABLE cron_job');
        $this->addSql('DROP TABLE cron_report');
        $this->addSql('ALTER TABLE card DROP FOREIGN KEY FK_161498D354FFFB0B');
        $this->addSql('ALTER TABLE card CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE status_key status_key INT UNSIGNED DEFAULT NULL, CHANGE number number VARCHAR(16) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE sum sum DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE card ADD CONSTRAINT card_ibfk_1 FOREIGN KEY (status_key) REFERENCES card_status (id) ON UPDATE CASCADE');
        $this->addSql('ALTER TABLE card RENAME INDEX uniq_161498d396901f54 TO number');
        $this->addSql('ALTER TABLE card_status CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL');
    }
}
