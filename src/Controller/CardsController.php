<?php
/**
 * Created by PhpStorm.
 * User: Roberts
 * Date: 01.12.2018
 * Time: 10:15
 */

namespace App\Controller;

use App\Entity\Card;
use App\Entity\CardStatus;
use App\Entity\Transactions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CardsController extends Controller
{
    /**
     * @Route("/", name="main_page")
     */
    public function listAction()
    {

        $cards = $this->getDoctrine()
            ->getRepository(Card::class)
            ->findAll();

        return $this->render('card_list.html.twig', ['cards' => $cards]);
    }

    /**
     * @Route("/card/{id}", name="card_profile")
     * int $id
     */
    public function viewCardProfileAction($id)
    {

        $card = $this->getDoctrine()
            ->getRepository(Card::class)
            ->find($id);

        $purchaceHistory = $this->getDoctrine()
            ->getRepository(Transactions::class)
            ->findBy(['cardKey' => $id]);

        if (!$card) {
            throw $this->createNotFoundException('No card found with given id: ' . $id);
        }

        return $this->render('card_profile.html.twig', ['card' => $card, 'purchases' => $purchaceHistory]);
    }

    /**
     * @Route("/create_card", name="create_card")
     */
    public function createCardAction(Request $request)
    {
        $card = new Card();

        $form = $this->createFormBuilder($card)
            ->add('number', TextType::class)
            ->add('serie', TextType::class, ['label' => 'Series Nr.'])
            ->add('dateOfExpiry', DateTimeType::class, ['label' => 'Expiration Date'])
            ->add('sum', MoneyType::class, ['label' => 'Balance'])
            ->add('statusKey', EntityType::class, ['class' => CardStatus::class, 'choice_label' => 'code', 'label' => 'Status'])
            ->add('save', SubmitType::class, ['label' => 'Create Card'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $card = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($card);
            $entityManager->flush();

            return $this->redirectToRoute('main_page');
        }

        return $this->render('card_creation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/generate_cards", name="cards_generation")
     */
    public function generateCardsAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('serie', TextType::class, ['label' => 'Series Nr.'])
            ->add('amount', NumberType::class)
            ->add('activePeriod', ChoiceType::class, [
                'choices' => [
                    '3 Months' => '3 month',
                    '6 Months' => '6 month',
                    '1 Year' => '1 year',
                    '2 Years' => '2 year'
                ]
            ])
            ->add('statusKey', EntityType::class, [
                'class' => CardStatus::class,
                'choice_label' => 'code',
                'label' => 'Status'
            ])
            ->add('save', SubmitType::class, ['label' => 'Generate Cards'])
            ->getForm();

        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $highestNumberInSeries = $this->getDoctrine()->getRepository(Card::class)->findHighestCardNumberForSeries($data['serie']);
            $highestNumberInSeries = $highestNumberInSeries['number'];

            //find if we already have cards in given series
            if (!$highestNumberInSeries) {
                $highestNumberInSeries = $data['serie'].'000000000000';
            } else {
                $highestNumberInSeries = $highestNumberInSeries+1;
            }

            //calculate date expiry date
            $expiryDate = (new \DateTime())->add(\DateInterval::createFromDateString($data['activePeriod']));
            $entityManager = $this->getDoctrine()->getManager();

            for ($i = 1; $i <= $data['amount']; $i++) {
                $card = new Card();
                $card->setNumber($highestNumberInSeries)
                    ->setDateOfExpiry($expiryDate)
                    ->setSerie($data['serie'])
                    ->setStatusKey($data['statusKey']);

                $highestNumberInSeries++;

                $entityManager->persist($card);
            }

            $entityManager->flush();
            $entityManager->clear();

            return $this->redirectToRoute('main_page');
        }

        return $this->render('card_creation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete_card/{id}", name = "delete_card")
     */
    public function deleteCardAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $card = $entityManager->getRepository(Card::class)->find($id);

        if (!$card) {
            throw $this->createNotFoundException('No card found with given id: ' . $id);
        }

        $entityManager->remove($card);
        $entityManager->flush();

        return $this->redirectToRoute('main_page');
    }

    /**
     * @Route("/delete_all_cards", name = "delete_all_cards")
     */
    public function deleteAllCards() {
        $entityManager = $this->getDoctrine()->getManager();
        $cards = $entityManager->getRepository(Card::class)->findAll();

        foreach ($cards as $card) {

            $transactions = $this->getDoctrine()->getRepository(Transactions::class)->findBy(['cardKey' => $card]);

            if ($transactions) {
                continue;
            }

            $entityManager->remove($card);
        }

        $entityManager->flush();

        return $this->redirectToRoute('main_page');
    }

    /**
     * @Route("/search_cards", name = "search_cards")
     */
    public function searchCardsAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'status' => 'Error',
                'message' => 'Not an http request'),
                400);
        }

        $cardsRepository = $this->getDoctrine()->getRepository(Card::class);

        $cards = null;

        $searchParams = json_decode($request->get('query_params'));

        $cards = $cardsRepository->createSearchQuery($searchParams);

        $response = [
            'code' => 200,
            'response' => $this->render("cards_table.twig", ['cards' => $cards])->getContent(),
        ];

        return new JsonResponse($response);
    }
}