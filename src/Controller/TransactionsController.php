<?php
/**
 * Created by PhpStorm.
 * User: Roberts
 * Date: 02.12.2018
 * Time: 21:16
 */

namespace App\Controller;

use App\Entity\Card;
use App\Entity\Products;
use App\Entity\Transactions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class TransactionsController extends Controller
{
    /**
     * @Route("/create_transaction", name="create_transaction")
     */
    public function createTransactionAction(Request $request)
    {
        $transaction = new Transactions();

        $form = $this->createFormBuilder($transaction)
            ->add('cardKey', EntityType::class, ['class' => Card::class, 'choice_label' => 'number', 'label' => 'Card'])
            ->add('productKey', EntityType::class, ['class' => Products::class, 'choice_label' => 'title', 'label' => 'Product name'])
            ->add('save', SubmitType::class, ['label' => 'Purchase item'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transaction = $form->getData();
            $transaction->setPurchaseDate(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();

            $card = $transaction->getCardKey();
            $card->setDateOfUse(new \DateTime());

            $entityManager->persist($transaction);
            $entityManager->persist($card);
            $entityManager->flush();

            return $this->redirectToRoute('main_page');
        }

        return $this->render('card_creation.html.twig', ['form' => $form->createView()]);
    }
}