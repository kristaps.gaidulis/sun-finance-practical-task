$(document).on("click", '#search-button', function (event) {
    var queryParams = {};
    queryParams['card_number'] = $("#card_number").val();
    queryParams['card_series'] = $("#card_series").val();
    queryParams['balance_value'] = $("#balance").val();
    queryParams['balance_comparison'] = $("#balance-comparison-selector").find(":selected").val();
    queryParams['exp_date_value'] = $("#expiration-date-search-input").val();
    queryParams['exp_date_comparison'] = $("#expiration-date-comparison-selector").find(":selected").val();
    queryParams['use_date_value'] = $("#last-used-date-search-input").val();
    queryParams['use_date_comparison'] = $("#last-used-date-comparison-selector").find(":selected").val();
    queryParams['status'] = $("#search-select").find(":selected").val();

    var jsonParams = JSON.stringify(queryParams);

    $.ajax({
        url: "/search_cards",
        type: 'POST',
        data: {
            query_params: jsonParams,
        },
        dataType: 'json',
        success: function (data, status) {
            $(".all-cards-table").html(data['response']);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log('Ajax request failed');
        }
    })
});